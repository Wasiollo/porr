#include <MatrixFunctionsVectorized.h>
#include "CholeskyVectorized.h"

#include "gtest/gtest.h"

class CholeskyVectorizedTest : public ::testing::Test {

protected:
    virtual void SetUp() {
    };

    virtual void TearDown() {
    };
};


TEST_F(CholeskyVectorizedTest, rows_lower_triangular) {
    CholeskyVectorized choleskyThreads;
    MatrixFunctionsVectorized matrixFunctionsThreads;

    int size = 3;

    double** source = matrixFunctionsThreads.allocateMatrix(size);
    double** lower = matrixFunctionsThreads.allocateMatrix(size);
    double** result = matrixFunctionsThreads.allocateMatrix(size);


    source[0][0] = 4;
    source[0][1] = 12;
    source[0][2] = -16;
    source[1][0] = 12;
    source[1][1] = 37;
    source[1][2] = -43;
    source[2][0] = -16;
    source[2][1] = -43;
    source[2][2] = 98;

    result[0][0] = 2;
    result[0][1] = 0;
    result[0][2] = 0;
    result[1][0] = 6;
    result[1][1] = 1;
    result[1][2] = 0;
    result[2][0] = -8;
    result[2][1] = 5;
    result[2][2] = 3;

    choleskyThreads.calculateRowsLowerMatrix(source, lower, size);

    EXPECT_EQ(lower[0][0], result[0][0]);
    EXPECT_EQ(lower[0][1], result[0][1]);
    EXPECT_EQ(lower[0][2], result[0][2]);
    EXPECT_EQ(lower[1][0], result[1][0]);
    EXPECT_EQ(lower[1][1], result[1][1]);
    EXPECT_EQ(lower[1][2], result[1][2]);
    EXPECT_EQ(lower[2][0], result[2][0]);
    EXPECT_EQ(lower[2][1], result[2][1]);
    EXPECT_EQ(lower[2][2], result[2][2]);

    matrixFunctionsThreads.deallocateMatrix(source, size);
    matrixFunctionsThreads.deallocateMatrix(lower, size);
    matrixFunctionsThreads.deallocateMatrix(result, size);
}

TEST_F(CholeskyVectorizedTest, columns_lower_triangular) {
    CholeskyVectorized choleskyThreads;
    MatrixFunctionsVectorized matrixFunctionsThreads;

    int size = 3;

    double** source = matrixFunctionsThreads.allocateMatrix(size);
    double** lower = matrixFunctionsThreads.allocateMatrix(size);
    double** result = matrixFunctionsThreads.allocateMatrix(size);


    source[0][0] = 4;
    source[0][1] = 12;
    source[0][2] = -16;
    source[1][0] = 12;
    source[1][1] = 37;
    source[1][2] = -43;
    source[2][0] = -16;
    source[2][1] = -43;
    source[2][2] = 98;

    result[0][0] = 2;
    result[0][1] = 0;
    result[0][2] = 0;
    result[1][0] = 6;
    result[1][1] = 1;
    result[1][2] = 0;
    result[2][0] = -8;
    result[2][1] = 5;
    result[2][2] = 3;

    choleskyThreads.calculateColumnsLowerMatrix(source, lower, size);

    EXPECT_EQ(lower[0][0], result[0][0]);
    EXPECT_EQ(lower[0][1], result[0][1]);
    EXPECT_EQ(lower[0][2], result[0][2]);
    EXPECT_EQ(lower[1][0], result[1][0]);
    EXPECT_EQ(lower[1][1], result[1][1]);
    EXPECT_EQ(lower[1][2], result[1][2]);
    EXPECT_EQ(lower[2][0], result[2][0]);
    EXPECT_EQ(lower[2][1], result[2][1]);
    EXPECT_EQ(lower[2][2], result[2][2]);

    matrixFunctionsThreads.deallocateMatrix(source, size);
    matrixFunctionsThreads.deallocateMatrix(lower, size);
    matrixFunctionsThreads.deallocateMatrix(result, size);
}

TEST_F(CholeskyVectorizedTest, columns_rows_lower_triangular_compare) {
    CholeskyVectorized choleskyThreads;
    MatrixFunctionsVectorized matrixFunctionsThreads;

    int size = 3;

    double** source = matrixFunctionsThreads.allocateMatrix(size);
    double** lowerRows = matrixFunctionsThreads.allocateMatrix(size);
    double** lowerColumns = matrixFunctionsThreads.allocateMatrix(size);

    source[0][0] = 4;
    source[0][1] = 12;
    source[0][2] = -16;
    source[1][0] = 12;
    source[1][1] = 37;
    source[1][2] = -43;
    source[2][0] = -16;
    source[2][1] = -43;
    source[2][2] = 98;

    choleskyThreads.calculateRowsLowerMatrix(source, lowerRows, size);
    choleskyThreads.calculateColumnsLowerMatrix(source, lowerColumns, size);

    EXPECT_EQ(lowerRows[0][0], lowerColumns[0][0]);
    EXPECT_EQ(lowerRows[0][1], lowerColumns[0][1]);
    EXPECT_EQ(lowerRows[0][2], lowerColumns[0][2]);
    EXPECT_EQ(lowerRows[1][0], lowerColumns[1][0]);
    EXPECT_EQ(lowerRows[1][1], lowerColumns[1][1]);
    EXPECT_EQ(lowerRows[1][2], lowerColumns[1][2]);
    EXPECT_EQ(lowerRows[2][0], lowerColumns[2][0]);
    EXPECT_EQ(lowerRows[2][1], lowerColumns[2][1]);
    EXPECT_EQ(lowerRows[2][2], lowerColumns[2][2]);

    matrixFunctionsThreads.deallocateMatrix(source, size);
    matrixFunctionsThreads.deallocateMatrix(lowerRows, size);
    matrixFunctionsThreads.deallocateMatrix(lowerColumns, size);
}
