#include "MatrixFunctions.h"

#include "gtest/gtest.h"

class MatrixFunctionsTest : public ::testing::Test {

protected:
    virtual void SetUp() {
    };

    virtual void TearDown() {
    };
};

TEST_F(MatrixFunctionsTest, allocation_matrix_test) {
    MatrixFunctions matrixFunctions;
    int size = 3;

    double **a = matrixFunctions.allocateMatrix(3);

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            a[i][j] = i * size + j + 1;
        }
    }
    double b[3][3] = {
            {1, 2, 3},
            {4, 5, 6},
            {7, 8, 9}
    };

    EXPECT_EQ(a[0][0], b[0][0]);
    EXPECT_EQ(a[0][1], b[0][1]);
    EXPECT_EQ(a[0][2], b[0][2]);
    EXPECT_EQ(a[1][0], b[1][0]);
    EXPECT_EQ(a[1][1], b[1][1]);
    EXPECT_EQ(a[1][2], b[1][2]);
    EXPECT_EQ(a[2][0], b[2][0]);
    EXPECT_EQ(a[2][1], b[2][1]);
    EXPECT_EQ(a[2][2], b[2][2]);

    matrixFunctions.deallocateMatrix(a, size);
}

TEST_F(MatrixFunctionsTest, multipy_matrix_test) {
    MatrixFunctions matrixFunctions;
    int size = 3;

    double** a = matrixFunctions.allocateMatrix(size);
    double** b = matrixFunctions.allocateMatrix(size);

    double** multiplyResult = matrixFunctions.allocateMatrix(size);
    double** expectedResult = matrixFunctions.allocateMatrix(size);


    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            a[i][j] = i * size + j + 1;
        }
    }

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            b[i][j] = i * size + j + 10;
        }
    }

    expectedResult[0][0] = 84;
    expectedResult[0][1] = 90;
    expectedResult[0][2] = 96;
    expectedResult[1][0] = 201;
    expectedResult[1][1] = 216;
    expectedResult[1][2] = 231;
    expectedResult[2][0] = 318;
    expectedResult[2][1] = 342;
    expectedResult[2][2] = 366;

    matrixFunctions.multiplyMatrix(a, b, multiplyResult, size);

    EXPECT_EQ(multiplyResult[0][0], expectedResult[0][0]);
    EXPECT_EQ(multiplyResult[0][1], expectedResult[0][1]);
    EXPECT_EQ(multiplyResult[0][2], expectedResult[0][2]);
    EXPECT_EQ(multiplyResult[1][0], expectedResult[1][0]);
    EXPECT_EQ(multiplyResult[1][1], expectedResult[1][1]);
    EXPECT_EQ(multiplyResult[1][2], expectedResult[1][2]);
    EXPECT_EQ(multiplyResult[2][0], expectedResult[2][0]);
    EXPECT_EQ(multiplyResult[2][1], expectedResult[2][1]);
    EXPECT_EQ(multiplyResult[2][2], expectedResult[2][2]);

    matrixFunctions.deallocateMatrix(a, size);
    matrixFunctions.deallocateMatrix(b, size);
    matrixFunctions.deallocateMatrix(multiplyResult, size);
    matrixFunctions.deallocateMatrix(expectedResult, size);
}

TEST_F(MatrixFunctionsTest, transpose_matrix_test) {
    MatrixFunctions matrixFunctions;
    int size = 3;

    double** source = matrixFunctions.allocateMatrix(size);
    double** transposeResult = matrixFunctions.allocateMatrix(size);
    double** expectedResult = matrixFunctions.allocateMatrix(size);

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            source[i][j] = i * size + j + 1;
        }
    }

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            expectedResult[j][i] = i * size + j + 1;
        }
    }

    matrixFunctions.transposeMatrix(source, transposeResult, size);

    EXPECT_EQ(transposeResult[0][0], expectedResult[0][0]);
    EXPECT_EQ(transposeResult[0][1], expectedResult[0][1]);
    EXPECT_EQ(transposeResult[0][2], expectedResult[0][2]);
    EXPECT_EQ(transposeResult[1][0], expectedResult[1][0]);
    EXPECT_EQ(transposeResult[1][1], expectedResult[1][1]);
    EXPECT_EQ(transposeResult[1][2], expectedResult[1][2]);
    EXPECT_EQ(transposeResult[2][0], expectedResult[2][0]);
    EXPECT_EQ(transposeResult[2][1], expectedResult[2][1]);
    EXPECT_EQ(transposeResult[2][2], expectedResult[2][2]);

    matrixFunctions.deallocateMatrix(source, size);
    matrixFunctions.deallocateMatrix(transposeResult, size);
    matrixFunctions.deallocateMatrix(expectedResult, size);
}

TEST_F(MatrixFunctionsTest, transpose_matrix_with_generator_test) {
    MatrixFunctions matrixFunctions;
    int size = 3;

    double** source = matrixFunctions.allocateMatrix(size);
    double** transposeResult = matrixFunctions.allocateMatrix(size);

    matrixFunctions.loadMatrixFromFile(source, size);
    matrixFunctions.transposeMatrix(source, transposeResult, size);

    EXPECT_EQ(source[0][0], transposeResult[0][0]);
    EXPECT_EQ(source[0][1], transposeResult[1][0]);
    EXPECT_EQ(source[0][2], transposeResult[2][0]);
    EXPECT_EQ(source[1][0], transposeResult[0][1]);
    EXPECT_EQ(source[1][1], transposeResult[1][1]);
    EXPECT_EQ(source[1][2], transposeResult[2][1]);
    EXPECT_EQ(source[2][0], transposeResult[0][2]);
    EXPECT_EQ(source[2][1], transposeResult[1][2]);
    EXPECT_EQ(source[2][2], transposeResult[2][2]);

    matrixFunctions.deallocateMatrix(source, size);
    matrixFunctions.deallocateMatrix(transposeResult, size);
}

TEST_F(MatrixFunctionsTest, deep_copy_matrix) {
    MatrixFunctions matrixFunctions;
    int size = 3;

    double** source = matrixFunctions.allocateMatrix(size);
    double** copyResult = matrixFunctions.allocateMatrix(size);

    matrixFunctions.loadMatrixFromFile(source, size);

    matrixFunctions.deepCopyMatrix(source, copyResult, size);

    EXPECT_EQ(source[0][0], copyResult[0][0]);
    EXPECT_EQ(source[0][1], copyResult[0][1]);
    EXPECT_EQ(source[0][2], copyResult[0][2]);
    EXPECT_EQ(source[1][0], copyResult[1][0]);
    EXPECT_EQ(source[1][1], copyResult[1][1]);
    EXPECT_EQ(source[1][2], copyResult[1][2]);
    EXPECT_EQ(source[2][0], copyResult[2][0]);
    EXPECT_EQ(source[2][1], copyResult[2][1]);
    EXPECT_EQ(source[2][2], copyResult[2][2]);

    matrixFunctions.deallocateMatrix(source, size);
    matrixFunctions.deallocateMatrix(copyResult, size);
}