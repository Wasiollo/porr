#!/bin/bash

BUILD_DIR=./build
CHOLESKY_EXE=./build/src/cholesky/cholesky
CHOLESKY_VECTORIZED_EXE=./build/src/cholesky_vectorized/cholesky_vectorized
CHOLESKY_THREADS_EXE=./build/src/cholesky_threads/cholesky_threads
CHOLESKY_OPEN_MP_EXE=./build/src/cholesky_open_mp/cholesky_open_mp

if [ ! -d "$BUILD_DIR" ] || [ ! -f "$CHOLESKY_EXE" ] || [ ! -f "$CHOLESKY_VECTORIZED_EXE" ] || [ ! -f "$CHOLESKY_THREADS_EXE" ] || [ ! -f "$CHOLESKY_OPEN_MP_EXE" ]; then
  echo "Can't perform time testing"
  exit 1
fi

echo "METHOD , TIME , MATRICES AMOUNT , MATRIX SIZE , TIME PER MATRIX" > result.csv

# i = number of matrices
# j = matrix size

for i in 1
do
  for j in 2 5 10 50 100 500 1000 2000 5000 10000
  do
    $CHOLESKY_EXE $j $i 1 >> result.csv
    $CHOLESKY_VECTORIZED_EXE $j $i 1 >> result.csv
    $CHOLESKY_THREADS_EXE $j $i 1 >> result.csv
    $CHOLESKY_OPEN_MP_EXE $j $i 1 >> result.csv
  done
done
