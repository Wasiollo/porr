#include <mpi.h>
#include <stdio.h>
#include <iostream>
#include <math.h>
#include <chrono>
#include <unordered_map>

#include "CholeskyOpenMPI.h"

void calculateLowerMatrixPartial(double* partialSource, double* partialLower, int size, int colIndex) {
    for (int i = colIndex; i < size; i++) {
        partialSource[i] -= partialLower[i] * partialLower[colIndex];
    }
}

int main(int argc, char *argv[]) {
    // Unique rank is assigned to each process in a communicator
    int rank;
    int ranksCount;
    char name[80];
    int length;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    MPI_Comm_size(MPI_COMM_WORLD, &ranksCount);

    MPI_Get_processor_name(name, &length);


    CholeskyOpenMPI cholesky(rank, ranksCount);

    int matSize = 2000;
    std::string filename = std::string("../../data/") + std::to_string(matSize) + std::string(".txt");

    std::cout << "Matrix size: " << matSize << std::endl;

    Matrix source = Matrix(matSize);
    source.loadFromFile(filename);

    cholesky.runCholeskyRowsAlgorithm(source);

    cholesky.runCholeskyColumnsAlgorithm(source);

    // Terminate MPI execution environment
    MPI_Finalize();
}