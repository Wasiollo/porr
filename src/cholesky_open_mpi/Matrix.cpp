#include "Matrix.h"
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>

Matrix::Matrix(size_t size, bool memoryColumnLayout) : matSize_(size), memoryColumnLayout_(memoryColumnLayout) {
    data_.resize(size*size);

    for (int i = 0 ; i < size*size ; ++i){
        data_[i] = 0.0;
    }
}

Matrix::Matrix(const std::string& filename, size_t size, bool memoryColumnLayout) : matSize_(size), memoryColumnLayout_(memoryColumnLayout) {
    loadFromFile(filename);
}

void Matrix::loadFromFile(const std::string& filename) {

    std::ifstream input(filename);

    std::string s;
    for (int i = 0; i < matSize_; i++)
    {
        std::getline(input, s);
        std::istringstream iss(s);

        std::string num;
        int j = 0;
        while (std::getline(iss, num, ',')){
            get(i,j) = std::stod(num);
            ++j;
        }
    }
}

void Matrix::print() {
     for(int i = 0 ; i < matSize_; ++i) {
        for(int j = 0 ; j < matSize_; ++j) {
            std::cout << "[" << i << "]" << "[" << j << "]" << get(i,j) << std::endl;
        }   
    }
}

void Matrix::transpose() {

    std::vector<double> result(matSize_ * matSize_);
    for (int i = 0; i < matSize_; ++i) {
        for (int j = 0; j < matSize_; ++j) {
            result[j*matSize_ + i] = get(i,j);
        }
    }

    std::memcpy(data_.data(), result.data(), matSize_ * matSize_);
}

Matrix Matrix::transopseAndCopy(Matrix& mat, size_t size) {
    Matrix result(size);
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            result.get(j,i) = mat.get(i,j);
        }
    }

    return result;
}

double& Matrix::get(size_t r, size_t c) {
    if( memoryColumnLayout_) {
        return data_[c*matSize_ + r];
    } else {
        return data_[r*matSize_ + c];
    }
}

double* Matrix::data() {
    return data_.data();
}

double Matrix::frobeniusNorm() {
    double sum = 0;
    for (int i = 0; i < matSize_; ++i) {
        for (int j = 0; j < matSize_; ++j) {
            sum += (get(i,j) * get(i,j));
        }
    }

    return sqrt(sum);
}

Matrix& Matrix::operator=(Matrix& other) {
    memcpy(data(), other.data(), matSize_ * matSize_ * sizeof(double));

    return *this;
}

Matrix Matrix::operator-(Matrix& other) {
    Matrix result(matSize_);
    for(int i = 0 ; i < matSize_ ; ++i){
        for (int j = 0 ; j < matSize_ ; ++j){
            result.get(i,j) = get(i,j) - other.get(i,j);
        }
    }

    return result;
}

Matrix Matrix::operator*(Matrix& other) {
    Matrix result(matSize_);
    for (int i = 0; i < matSize_; ++i) {
        for (int j = 0; j < matSize_; ++j) {
            double calculatedSum = 0;
            for (int k = 0; k < matSize_; ++k) {
                calculatedSum += get(i,k) * other.get(k,j);
            }
            result.get(i,j) = calculatedSum;
        }
    }

    return result;
}

size_t Matrix::size() {
    return matSize_;
}