#pragma once

#include <cstdio>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <chrono>
#include "Matrix.h"

class CholeskyOpenMPI {
public:
    CholeskyOpenMPI(int rank, int ranksCount);

    struct CholeskyResultType {
        double time;
        double frobeniusNorm;
    };

    struct SplitProps {
        std::vector<int> displs;
        std::vector<int> sendCounts;
        int batchSize;
    };

    SplitProps calculateSplitProps(size_t matSize);

    CholeskyResultType runCholeskyRowsAlgorithm(Matrix matrix);

    CholeskyResultType runCholeskyColumnsAlgorithm(Matrix matrix);


private:
    int rank_;
    int ranksCount_;
};