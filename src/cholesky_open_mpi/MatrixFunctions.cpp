#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>
#include "MatrixFunctions.h"

MatrixFunctions::MatrixFunctions() = default;

double *MatrixFunctions::allocateArray(int size) {
    auto *array = new double[size];

    for (int i = 0 ; i < size ; ++i){
        array[i] = 0;
    }

    return array;
}

double **MatrixFunctions::allocateMatrix(int size) {
    auto **matrix = new double *[size];

    for (int i = 0; i < size; ++i) {
        matrix[i] = new double[size];
    }

    for (int i = 0 ; i < size ; ++i){
        for (int j = 0 ; j < size ; ++j){
            matrix[i][j] = 0;
        }
    }

    return matrix;
}

double ***MatrixFunctions::allocateNMatrices(int n, int size) {
    auto ***nMatrices = new double **[n];
    for (int i = 0; i < n; ++i) {
        nMatrices[i] = allocateMatrix(size);
    }
    return nMatrices;
}

void MatrixFunctions::deallocateArray(const double *array) {
    delete[] array;
}

void MatrixFunctions::deallocateMatrix(double **matrix, int size) {
    for (int i = 0; i < size; ++i) {
        delete[] matrix[i];
    }
    delete[] matrix;
}

void MatrixFunctions::deallocateNMatrices(double ***nMatrices, int n, int size) {
    for (int i = 0; i < n; ++i) {
        this->deallocateMatrix(nMatrices[i], size);
    }
    delete[] nMatrices;
}

void MatrixFunctions::multiplyMatrix(double **a, double **b, double **result, int size) {
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            double calculatedSum = 0;
            for (int k = 0; k < size; ++k) {
                calculatedSum += a[i][k] * b[k][j];
            }
            result[i][j] = calculatedSum;
        }
    }
}

void MatrixFunctions::transposeMatrix(double **source, double **result, int size) {
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            result[j][i] = source[i][j];
        }
    }
}

void MatrixFunctions::generateBArray(double *result, int size) {
    for (int i = 0; i < size; ++i) {
        result[i] = this->randValue();
    }
}

void MatrixFunctions::printArray(double *array, int size) {
    std::cout << std::setw(12) << " Array " << std::endl;
    for (int i = 0; i < size; ++i) {
        std::cout << std::setw(12) << array[i] << std::endl;
    }
    std::cout << std::endl;
}

void MatrixFunctions::printMatrix(double **matrix, int size) {
    std::cout << std::setw(12) << " Matrix " << std::endl;

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            auto kek = std::isnan(matrix[i][j]) ? 0 : matrix[i][j];
            std::cout << std::setw(12) << kek << '\t';
        }
        std::cout << std::endl;
    }

}

double **MatrixFunctions::allocateNArrays(int n, int size) {
    auto **matrix = new double *[n];

    for (int i = 0; i < n; ++i) {
        matrix[i] = new double[size];
    }

    for (int i = 0 ; i < n ; ++i){
        for (int j = 0 ; j < size ; ++j){
            matrix[i][j] = 0;
        }
    }

    return matrix;
}

void MatrixFunctions::deallocateNArrays(double **nArrays, int n) {
    for (int i = 0; i < n; ++i) {
        delete[] nArrays[i];
    }
    delete[] nArrays;
}

double MatrixFunctions::randValue() {
    int randRange = 200;
//    randRange *= 1000; // uncomment this and two line below to generate float numbers
    double randResult = rand() % (randRange); // three digits after comma
//    randResult /= 1000; // // uncomment this and two line above to generate float numbers
    randResult -= 100;
    return randResult;
}

void MatrixFunctions::deepCopyMatrix(double **source, double **target, int size) {
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            target[i][j] = source[i][j];
        }
    }
}

void MatrixFunctions::deepCopyArray(double *source, double *target, int size) {
    for (int i = 0; i < size; ++i) {
        target[i] = source[i];
    }
}

void MatrixFunctions::subtractMatrices(double **a, double **b, double **result, int size) {
    for(int i = 0 ; i < size ; ++i){
        for (int j = 0 ; j <size ; ++j){
            result[i][j] = a[i][j] - b[i][j];
        }
    }
}

double MatrixFunctions::frobeniusNorm(double **matrix, int size) {
    double sum = 0;
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            sum += (matrix[i][j] * matrix[i][j]);
//            std::cout<<"i: " << i << " j: " << j << " matrix[i][j] " << matrix[i][j] << " sum: " << sum << std::endl;
        }
    }

    return sqrt(sum);
}

void MatrixFunctions::loadMatrixFromFile(double **result, int size) {

    std::string filename = "../../data/" + std::to_string(size) + ".txt";

    std::ifstream input(filename);

    std::string s;
    for (int i = 0; i < size; i++)
    {
        std::getline(input, s);
        std::istringstream iss(s);

        std::string num;
        int j = 0;
        while (std::getline(iss, num, ',')){
            result[i][j] = std::stod(num);
            ++j;
        }
    }

}



