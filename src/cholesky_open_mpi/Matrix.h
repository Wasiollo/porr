#include <string>
#include <vector>

class Matrix {
    public:
    Matrix(size_t size, bool memoryColumnLayout = true);
    Matrix(const std::string& filename, size_t size, bool memoryColumnLayout = true);

    Matrix& operator=(Matrix& other);
    Matrix operator-(Matrix& other);
    Matrix operator*(Matrix& other);

    double& get(size_t r, size_t c);

    double* data();
    void transpose();

    void print();

    void loadFromFile(const std::string& filename);

    double frobeniusNorm();

    static Matrix transopseAndCopy(Matrix& mat, size_t size);

    size_t size();
private:
    std::vector<double> data_;
    size_t matSize_;
    bool memoryColumnLayout_;
};