#include "CholeskyOpenMPI.h"
#include <mpi.h>
#include <unordered_map>

CholeskyOpenMPI::CholeskyOpenMPI(int rank, int ranksCount) : rank_(rank), ranksCount_(ranksCount) {

}

CholeskyOpenMPI::SplitProps CholeskyOpenMPI::calculateSplitProps(size_t matSize) {
    SplitProps props;

    props.displs.resize(ranksCount_);
    for(int i=0; i < ranksCount_;++i) {
        props.displs[i] = 0;
    }
    props.sendCounts.resize(ranksCount_);
    for(int i=0; i < ranksCount_; ++i) {
        props.sendCounts[i] = 0;
    }

    div_t div = std::div(matSize, ranksCount_);
    props.batchSize = div.quot;

    int offset = 0;

    for(int r = 0; r < ranksCount_; ++r) {
        props.displs[r] = offset;
        if(r == ranksCount_ - 1) {
            props.sendCounts[r] = (props.batchSize + div.rem) * matSize;
            offset += (props.batchSize + div.rem) * matSize;
        } else {
            props.sendCounts[r] = props.batchSize * matSize;
            offset += props.batchSize * matSize;
        }
    }

    return props;
}

CholeskyOpenMPI::CholeskyResultType CholeskyOpenMPI::runCholeskyColumnsAlgorithm(Matrix source)  {
    int matSize = source.size();
    Matrix lower = Matrix(matSize);
    Matrix sourceCopy = Matrix(matSize);
    sourceCopy = source;

    SplitProps props = calculateSplitProps(matSize);

    /* START counting time for Cholesky columns method */
    std::chrono::_V2::system_clock::time_point choleskyColumnsStartTime ;
    if(rank_ == 0) {
        choleskyColumnsStartTime = std::chrono::high_resolution_clock::now();
    }

    for (int k = 0; k < matSize - 1; k++) {
        if(rank_ ==0) {
            lower.get(k,k) = sqrt(std::abs(source.get(k,k)));

            for (int i = k + 1; i < matSize; i++) {
                lower.get(i,k) = source.get(i,k) / lower.get(k,k);
            }

        }
        
        MPI_Bcast(lower.data(), matSize*matSize, MPI_DOUBLE, 0, MPI_COMM_WORLD);

        double* partialSource = new double[props.sendCounts[rank_]];
        MPI_Scatterv(source.data(), props.sendCounts.data() , props.displs.data(), MPI_DOUBLE, partialSource, props.sendCounts[rank_], MPI_DOUBLE, 0, MPI_COMM_WORLD );

        int colIndexStart = std::max(rank_ * props.batchSize, k+1);
        int colIndexEnd = std::min((rank_ * props.batchSize) + (props.sendCounts[rank_] / matSize), matSize);
        for(int j = colIndexStart; j < colIndexEnd; ++j) {
            for (int i = j; i < matSize; i++) {
                int sourceIndex = ((j-(rank_ * props.batchSize)) * matSize) + i;
                partialSource[sourceIndex] -= lower.get(i,k) * lower.get(j,k);
            }
        }

        

        MPI_Gatherv(partialSource, props.sendCounts[rank_], MPI_DOUBLE, source.data(), props.sendCounts.data() , props.displs.data(), MPI_DOUBLE, 0, MPI_COMM_WORLD);
        delete[] partialSource;

        MPI_Barrier(MPI_COMM_WORLD);

    }
    lower.get(matSize - 1,matSize - 1) = sqrt(std::abs(source.get(matSize - 1,matSize - 1)));

    CholeskyResultType result;
    if(rank_ == 0) {
        auto choleskyColumnsEndTime = std::chrono::high_resolution_clock::now();
        auto choleskyColumnsDuration = std::chrono::duration_cast<std::chrono::microseconds>(choleskyColumnsEndTime - choleskyColumnsStartTime);

        Matrix transLower = Matrix::transopseAndCopy(lower, lower.size());

        Matrix llt = lower * transLower;

        Matrix errorMatrix = sourceCopy - llt;

        double frobeniusNorm = errorMatrix.frobeniusNorm();

        std::cout << "Column Algorithm Time: " << choleskyColumnsDuration.count() << " Frobenius Norm: " << frobeniusNorm << std::endl;
        result.time = choleskyColumnsDuration.count();
        result.frobeniusNorm = frobeniusNorm;
    }
    return result;
}

CholeskyOpenMPI::CholeskyResultType CholeskyOpenMPI::runCholeskyRowsAlgorithm(Matrix source) {
    int matSize = source.size();

    SplitProps props = calculateSplitProps(matSize);

    //Create matrix that is stored row by row in memory
    Matrix lower = Matrix(matSize, false);

    std::unordered_map<int, std::vector<double>>cachedRows{};
    
    /* START counting time for Cholesky rows method */
    std::chrono::_V2::system_clock::time_point choleskyRowsStartTime;
    if(rank_ == 0) {
        choleskyRowsStartTime = std::chrono::high_resolution_clock::now();
    }

    double * partialLower = new double[props.sendCounts[rank_]];
    MPI_Scatterv(lower.data(), props.sendCounts.data() , props.displs.data(), MPI_DOUBLE, partialLower, props.sendCounts[rank_], MPI_DOUBLE, 0, MPI_COMM_WORLD );


    int rowIndexStart = rank_ * props.batchSize;
    int rowIndexEnd = (rank_ * props.batchSize) + (props.sendCounts[rank_] / matSize);

    for(int i = rowIndexStart; i < rowIndexEnd; ++i) {
        for (int j = 0; j <= i; j++) {
            int lowerStartIndex = ((i-(rank_ * props.batchSize)) * matSize);
            double sum = 0;

            if (j == i) {
                for (int k = 0; k < j; k++){
                    sum += pow(partialLower[lowerStartIndex + k], 2); //TODO 
                }
                partialLower[lowerStartIndex + j] = sqrt(std::abs(source.get(j,j) - sum));
                for(int r = rank_ + 1; r < ranksCount_; ++r) {
                    MPI_Send(&(partialLower[lowerStartIndex]), matSize, MPI_DOUBLE, r, j, MPI_COMM_WORLD);
                }

            } else {
                if( j < rowIndexEnd && j >= rowIndexStart) {
                    int startCalculatedIndex = ((j-(rank_ * props.batchSize)) * matSize);

                    for (int k = 0; k < j; k++) {
                        sum += (partialLower[lowerStartIndex + k] * partialLower[startCalculatedIndex + k]);
                    }
                    partialLower[lowerStartIndex + j] = (source.get(i,j) - sum) / partialLower[startCalculatedIndex + j];
                } else {
                    auto found = cachedRows.find(j);
                    if(found == cachedRows.end()) {
                        double calculatedLowerJ[matSize];
                        MPI_Recv(calculatedLowerJ, matSize, MPI_DOUBLE, MPI_ANY_SOURCE, j, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

                        cachedRows[j] = std::vector<double>(calculatedLowerJ, calculatedLowerJ + matSize);
                    }

                    double* calculatedLowerJ = cachedRows[j].data();

                    for (int k = 0; k < j; k++) {
                        sum += (partialLower[lowerStartIndex + k] * calculatedLowerJ[k]);
                    }
                    partialLower[lowerStartIndex + j] = (source.get(i,j) - sum) / calculatedLowerJ[j];
                }
            }
        }
    }

    MPI_Gatherv(partialLower, props.sendCounts[rank_], MPI_DOUBLE, lower.data(), props.sendCounts.data() , props.displs.data(), MPI_DOUBLE, 0, MPI_COMM_WORLD);

    delete[] partialLower;

    CholeskyResultType result;
    if(rank_ == 0) {
        auto choleskyRowsEndTime = std::chrono::high_resolution_clock::now();
        auto choleskyRowsDuration = std::chrono::duration_cast<std::chrono::microseconds>(choleskyRowsEndTime - choleskyRowsStartTime);

        Matrix transLower = Matrix::transopseAndCopy(lower, lower.size());

        Matrix llt = lower * transLower;

        Matrix errorMatrix = source - llt;

        double frobeniusNorm = errorMatrix.frobeniusNorm();

        result.frobeniusNorm = frobeniusNorm;
        result.time = choleskyRowsDuration.count();
        std::cout << "Row Algorithm Time: " << choleskyRowsDuration.count() << " Frobenius Norm: " << frobeniusNorm << std::endl;
    }
    return result;
}
