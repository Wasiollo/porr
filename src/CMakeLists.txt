cmake_minimum_required(VERSION 3.2)
project(porr)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

add_subdirectory(cholesky)
add_subdirectory(cholesky_vectorized)
add_subdirectory(cholesky_threads)
#add_subdirectory(cholesky_open_mpi)

