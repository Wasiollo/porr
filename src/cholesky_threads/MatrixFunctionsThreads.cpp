#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>
#include "MatrixFunctionsThreads.h"

MatrixFunctionsThreads::MatrixFunctionsThreads() = default;

double *MatrixFunctionsThreads::allocateArray(int size) {
    auto *array = new double[size];

    for (int i = 0 ; i < size ; ++i){
        array[i] = 0;
    }

    return array;
}

double **MatrixFunctionsThreads::allocateMatrix(int size) {
    auto **matrix = new double *[size];

    for (int i = 0; i < size; ++i) {
        matrix[i] = new double[size];
    }

    for (int i = 0 ; i < size ; ++i){
        for (int j = 0 ; j < size ; ++j){
            matrix[i][j] = 0;
        }
    }

    return matrix;
}

double ***MatrixFunctionsThreads::allocateNMatrices(int n, int size) {
    auto ***nMatrices = new double **[n];
    for (int i = 0; i < n; ++i) {
        nMatrices[i] = allocateMatrix(size);
    }
    return nMatrices;
}

void MatrixFunctionsThreads::deallocateArray(const double *array) {
    delete[] array;
}

void MatrixFunctionsThreads::deallocateMatrix(double **matrix, int size) {
    for (int i = 0; i < size; ++i) {
        delete[] matrix[i];
    }
    delete[] matrix;
}

void MatrixFunctionsThreads::deallocateNMatrices(double ***nMatrices, int n, int size) {
    for (int i = 0; i < n; ++i) {
        this->deallocateMatrix(nMatrices[i], size);
    }
    delete[] nMatrices;
}

void MatrixFunctionsThreads::multiplyMatrix(double **a, double **b, double **result, int size) {
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            double calculatedSum = 0;
            for (int k = 0; k < size; ++k) {
                calculatedSum += a[i][k] * b[k][j];
            }
            result[i][j] = calculatedSum;
        }
    }
}

void MatrixFunctionsThreads::transposeMatrix(double **source, double **result, int size) {
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            result[j][i] = source[i][j];
        }
    }
}

void MatrixFunctionsThreads::generateBArray(double *result, int size) {
    for (int i = 0; i < size; ++i) {
        result[i] = this->randValue();
    }
}

void MatrixFunctionsThreads::printArray(double *array, int size) {
    std::cout << std::setw(12) << " Array " << std::endl;
    for (int i = 0; i < size; ++i) {
        std::cout << std::setw(12) << array[i] << std::endl;
    }
    std::cout << std::endl;
}

void MatrixFunctionsThreads::printMatrix(double **matrix, int size) {
    std::cout << std::setw(12) << " Matrix " << std::endl;

    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            std::cout << std::setw(12) << matrix[i][j] << '\t';
        }
        std::cout << std::endl;
    }

}

double **MatrixFunctionsThreads::allocateNArrays(int n, int size) {
    auto **matrix = new double *[n];

    for (int i = 0; i < n; ++i) {
        matrix[i] = new double[size];
    }

    for (int i = 0 ; i < n ; ++i){
        for (int j = 0 ; j < size ; ++j){
            matrix[i][j] = 0;
        }
    }

    return matrix;
}

void MatrixFunctionsThreads::deallocateNArrays(double **nArrays, int n) {
    for (int i = 0; i < n; ++i) {
        delete[] nArrays[i];
    }
    delete[] nArrays;
}

double MatrixFunctionsThreads::randValue() {
    int randRange = 200;
//    randRange *= 1000; // uncomment this and two line below to generate float numbers
    double randResult = rand() % (randRange); // three digits after comma
//    randResult /= 1000; // // uncomment this and two line above to generate float numbers
    randResult -= 100;
    return randResult;
}

void MatrixFunctionsThreads::deepCopyMatrix(double **source, double **target, int size) {
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            target[i][j] = source[i][j];
        }
    }
}

void MatrixFunctionsThreads::deepCopyArray(double *source, double *target, int size) {
    for (int i = 0; i < size; ++i) {
        target[i] = source[i];
    }
}

void MatrixFunctionsThreads::subtractMatrices(double **a, double **b, double **result, int size) {
    for(int i = 0 ; i < size ; ++i){
        for (int j = 0 ; j <size ; ++j){
            result[i][j] = a[i][j] - b[i][j];
        }
    }
}

double MatrixFunctionsThreads::frobeniusNorm(double **matrix, int size) {
    double sum = 0;
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            sum += (matrix[i][j] * matrix[i][j]);
        }
    }

    return sqrt(sum);
}

void MatrixFunctionsThreads::loadMatrixFromFile(double **result, int size) {

    std::string filename = "../../data/" + std::to_string(size) + ".txt";

    std::ifstream input(filename);

    std::string s;
    for (int i = 0; i < size; i++)
    {
        std::getline(input, s);
        std::istringstream iss(s);

        std::string num;
        int j = 0;
        while (std::getline(iss, num, ',')){
            result[i][j] = std::stod(num);
            ++j;
        }
    }

}
