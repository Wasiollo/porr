#include "CholeskyThreads.h"
#include <unordered_map>

CholeskyThreads::CholeskyThreads() : threadPool_(8) {};

void CholeskyThreads::calculateColumnsLowerMatrix(double** array, double** lower, int size) {
    int batchSize = static_cast<int>(size / (threadPool_.getThreadsNum())) + 1;
    for (int k = 0; k < size - 1; k++) {
        lower[k][k] = sqrt(std::abs(array[k][k]));

        std::vector<std::future<void>> results;
        for (int i = k + 1; i < size; i++) {
            lower[i][k] = array[i][k] / lower[k][k];
        }

        for(int colIndex = k + 1; colIndex < size; colIndex += batchSize) {
            int last = std::min(size, colIndex + batchSize); 
            results.emplace_back(threadPool_.run([&array, &lower, last, colIndex, size, k] {
                for (int j = colIndex; j < last; j++) {
                    for (int i = j; i < size; i++) {
                        array[i][j] -= lower[i][k] * lower[j][k];
                    }
                }
            }));
        }
        for (auto&& result : results) {
            result.wait();
        }
        results.clear();
        
    }
    lower[size - 1][size - 1] = sqrt(std::abs(array[size - 1][size - 1]));
}

void CholeskyThreads::calculateRowsLowerMatrix(double **source, double **lower, int size)  {

    std::vector<std::future<void>> results;
    std::vector<bool> rowIsFinished(size, false);
    std::vector<std::condition_variable> rowConditionVar(size);
    std::vector<std::mutex> rowMutex(size);


    int batchSize = static_cast<int>(size / (threadPool_.getThreadsNum())) + 1;

    for(int index = 0; index < size; index += batchSize) {

        int last = std::min(size - 1, index + batchSize);
        results.emplace_back(threadPool_.run([&source, &lower, index, last, &rowMutex, &rowConditionVar, &rowIsFinished] {

            for (int i = index; i <= last; i++) {
                for (int j = 0; j <= i; j++) {
                    double sum = 0;

                    if (j == i) {
                        for (int k = 0; k < j; k++){
                            sum += pow(lower[j][k], 2);
                        }
                        lower[j][j] = sqrt(std::abs(source[j][j] - sum));
                        {
                            std::unique_lock<std::mutex> lock(rowMutex[j]);
                            rowIsFinished[i] = true;
                            rowConditionVar[i].notify_all();
                        }
                    } else {
                        {
                            std::unique_lock<std::mutex> lock(rowMutex[j]);
                            if(!rowIsFinished[j]) {
                                rowConditionVar[j].wait(lock);
                            }
                        }
                        for (int k = 0; k < j; k++) {
                            sum += (lower[i][k] * lower[j][k]);
                        }
                        lower[i][j] = (source[i][j] - sum) / lower[j][j];
                    }

                }
            }
        }));
    }

    for(auto && result : results) {
        result.wait();
    }

}

CholeskyThreads::CholeskyResultType
CholeskyThreads::runCholeskyThreadsColumnsAlgorithm(double **matrix, double *b, double *x, int size)  {
    MatrixFunctionsThreads matrixFunctionsThreads;

    // allocation of memory
    double** factorizedLowerMatrix = matrixFunctionsThreads.allocateMatrix(size);
    double** factorizedLowerMatrixTransposed = matrixFunctionsThreads.allocateMatrix(size);
    double* y = matrixFunctionsThreads.allocateArray(size);
    double ** matrixCopy = matrixFunctionsThreads.allocateMatrix(size);
    matrixFunctionsThreads.deepCopyMatrix(matrix, matrixCopy, size);
    // allocation matrices for frobenius norm calculation
    auto errorMatrix = matrixFunctionsThreads.allocateMatrix(size);
    auto lltMatrix = matrixFunctionsThreads.allocateMatrix(size); // L*L^T matrix

    /* START counting time for CholeskyThreads columns method */
    auto choleskyThreadsColumnsStartTime = std::chrono::high_resolution_clock::now();

    /* CORE PART OF CALCULATION */
    this->calculateColumnsLowerMatrix(matrix, factorizedLowerMatrix, size);

    /* END counting time for CholeskyThreads columns method */
    auto choleskyThreadsColumnsEndTime = std::chrono::high_resolution_clock::now();
    auto choleskyThreadsColumnsDuration = std::chrono::duration_cast<std::chrono::microseconds>(choleskyThreadsColumnsEndTime - choleskyThreadsColumnsStartTime);

    matrixFunctionsThreads.transposeMatrix(factorizedLowerMatrix, factorizedLowerMatrixTransposed, size);
    matrixFunctionsThreads.multiplyMatrix(factorizedLowerMatrix, factorizedLowerMatrixTransposed, lltMatrix, size);
    matrixFunctionsThreads.subtractMatrices(matrixCopy, lltMatrix, errorMatrix, size);

    auto frobeniusNorm = matrixFunctionsThreads.frobeniusNorm(errorMatrix, size);


    // deallocation of memory
    matrixFunctionsThreads.deallocateMatrix(lltMatrix, size);
    matrixFunctionsThreads.deallocateMatrix(errorMatrix, size);

    matrixFunctionsThreads.deallocateArray(y);
    matrixFunctionsThreads.deallocateMatrix(factorizedLowerMatrixTransposed, size);
    matrixFunctionsThreads.deallocateMatrix(factorizedLowerMatrix, size);

    auto time =  choleskyThreadsColumnsDuration.count();

    CholeskyResultType result;
    result.time = time;
    result.frobeniusNorm = frobeniusNorm;
    return result;
}

CholeskyThreads::CholeskyResultType
CholeskyThreads::runCholeskyThreadsRowsAlgorithm(double **matrix, double *b, double *x, int size) {
    MatrixFunctionsThreads matrixFunctionsThreads;

    // allocation of memory
    double** factorizedLowerMatrix = matrixFunctionsThreads.allocateMatrix(size);
    double** factorizedLowerMatrixTransposed = matrixFunctionsThreads.allocateMatrix(size);
    double* y = matrixFunctionsThreads.allocateArray(size);
    // allocation matrices for frobenius norm calculation
    auto errorMatrix = matrixFunctionsThreads.allocateMatrix(size);
    auto lltMatrix = matrixFunctionsThreads.allocateMatrix(size); // L*L^T matrix

    /* START counting time for CholeskyThreads rows method */
    auto choleskyThreadsRowsStartTime = std::chrono::high_resolution_clock::now();

    /* CORE PART OF CALCULATION */
    this->calculateRowsLowerMatrix(matrix, factorizedLowerMatrix, size);

    /* END counting time for CholeskyThreads rows method */
    auto choleskyThreadsRowsEndTime = std::chrono::high_resolution_clock::now();
    auto choleskyThreadsRowsDuration = std::chrono::duration_cast<std::chrono::microseconds>(choleskyThreadsRowsEndTime - choleskyThreadsRowsStartTime);

    matrixFunctionsThreads.transposeMatrix(factorizedLowerMatrix, factorizedLowerMatrixTransposed, size);
    matrixFunctionsThreads.multiplyMatrix(factorizedLowerMatrix, factorizedLowerMatrixTransposed, lltMatrix, size);
    matrixFunctionsThreads.subtractMatrices(matrix, lltMatrix, errorMatrix, size);

    auto frobeniusNorm = matrixFunctionsThreads.frobeniusNorm(errorMatrix, size);


    // deallocation of memory
    matrixFunctionsThreads.deallocateMatrix(lltMatrix, size);
    matrixFunctionsThreads.deallocateMatrix(errorMatrix, size);
    matrixFunctionsThreads.deallocateArray(y);
    matrixFunctionsThreads.deallocateMatrix(factorizedLowerMatrixTransposed, size);
    matrixFunctionsThreads.deallocateMatrix(factorizedLowerMatrix, size);

    auto time =  choleskyThreadsRowsDuration.count();

    CholeskyResultType result;
    result.time = time;
    result.frobeniusNorm = frobeniusNorm;
    return result;
}
