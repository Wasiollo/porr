#include <iostream>
#include <string>
#include <chrono>

#include <CholeskyThreads.h>
#include <MatrixFunctionsThreads.h>

int main(int argc, const char *argv[]) {
    CholeskyThreads choleskyThreads;
    MatrixFunctionsThreads matrixFunctionsThreads;

    int matrixSize = 49;
    int numberOfMatrices = 1;
    bool csvOutputType = false;

    if (argc > 1) {
        matrixSize = atoi(argv[1]);
    }
    if (argc > 2) {
        numberOfMatrices = atoi(argv[2]);
    }
    if (argc > 3) {
        if (atoi(argv[3]) == 1) {
            csvOutputType = true;
        }
    }

    double rowsTimeConsumed = 0;
    double rowsFrobeniusNorm = 0;
    double columnsTimeConsumed = 0;
    double columnsFrobeniusNorm = 0;

    /* MEMORY ALLOCATION */
    double ***arrayOfSourceMatricesForColumnsMethod = matrixFunctionsThreads.allocateNMatrices(numberOfMatrices, matrixSize);
    double ***arrayOfSourceMatricesForRowsMethod = matrixFunctionsThreads.allocateNMatrices(numberOfMatrices, matrixSize);
    double **arrayOfBArrays = matrixFunctionsThreads.allocateNArrays(numberOfMatrices, matrixSize); // can be reused
    double **arrayOfResultArraysForColumnsMethod = matrixFunctionsThreads.allocateNArrays(numberOfMatrices, matrixSize);
    double **arrayOfResultArraysForRowsMethod = matrixFunctionsThreads.allocateNArrays(numberOfMatrices, matrixSize);

    for (int i = 0; i < numberOfMatrices; ++i) {
        /* GENERATE MATRIX */
        matrixFunctionsThreads.loadMatrixFromFile(arrayOfSourceMatricesForColumnsMethod[i], matrixSize);
        /* DEEP COPY */
        matrixFunctionsThreads.deepCopyMatrix(arrayOfSourceMatricesForColumnsMethod[i], arrayOfSourceMatricesForRowsMethod[i], matrixSize);
        /* GENERATE B ARRAY */
        matrixFunctionsThreads.generateBArray(arrayOfBArrays[i], matrixSize);
    }

    /* ROWS METHOD EXECUTION */
    for (int i = 0; i < numberOfMatrices; ++i) {
        auto result = choleskyThreads.runCholeskyThreadsRowsAlgorithm(arrayOfSourceMatricesForRowsMethod[i],
                                                              arrayOfBArrays[i],
                                                              arrayOfResultArraysForRowsMethod[i],
                                                              matrixSize);
        rowsTimeConsumed += result.time;
        rowsFrobeniusNorm += result.frobeniusNorm;
    }

    /* COLUMNS METHOD EXECUTION */
    for (int i = 0; i < numberOfMatrices; ++i) {
        auto result = choleskyThreads.runCholeskyThreadsColumnsAlgorithm(arrayOfSourceMatricesForColumnsMethod[i],
                                                             arrayOfBArrays[i],
                                                             arrayOfResultArraysForColumnsMethod[i],
                                                             matrixSize);
        columnsTimeConsumed += result.time;
        columnsFrobeniusNorm +=result.frobeniusNorm;
    }

    /* MEMORY DEALLOCATION */
    matrixFunctionsThreads.deallocateNArrays(arrayOfResultArraysForRowsMethod, numberOfMatrices);
    matrixFunctionsThreads.deallocateNArrays(arrayOfResultArraysForColumnsMethod, numberOfMatrices);
    matrixFunctionsThreads.deallocateNArrays(arrayOfBArrays, numberOfMatrices);
    matrixFunctionsThreads.deallocateNMatrices(arrayOfSourceMatricesForRowsMethod, numberOfMatrices, matrixSize);
    matrixFunctionsThreads.deallocateNMatrices(arrayOfSourceMatricesForColumnsMethod, numberOfMatrices, matrixSize);

    if(!csvOutputType){
        std::cout << "CholeskyThreads factorization (columns method) take " << columnsTimeConsumed << " microseconds for "
                  << numberOfMatrices << " matrices with size " << matrixSize << "x" << matrixSize
                  << ". Per matrix: " << columnsTimeConsumed / numberOfMatrices << " microseconds."
                  << " Average Frobenius Norm: " << columnsFrobeniusNorm / numberOfMatrices << std::endl;

        std::cout << "CholeskyThreads factorization (rows method) take " << rowsTimeConsumed << " microseconds for "
                  << numberOfMatrices << " matrices with size " << matrixSize << "x" << matrixSize
                  << ". Per matrix: " << rowsTimeConsumed / numberOfMatrices << " microseconds."
                  << " Average Frobenius Norm: " << rowsFrobeniusNorm / numberOfMatrices << std::endl;

    } else {
        std::cout << "CholeskyThreads factorization (columns method)," << columnsTimeConsumed << ","
                  << numberOfMatrices << "," << matrixSize << "x" << matrixSize
                  << "," << columnsTimeConsumed / numberOfMatrices << "," << columnsFrobeniusNorm / numberOfMatrices
                  << std::endl;

        std::cout << "CholeskyThreads factorization (rows method)," << rowsTimeConsumed << ","
                  << numberOfMatrices << "," << matrixSize << "x" << matrixSize
                  << "," << rowsTimeConsumed / numberOfMatrices << "," << rowsFrobeniusNorm / numberOfMatrices
                  << std::endl;
    }
    return 0;
}
