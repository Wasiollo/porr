#pragma once

#include <cstdio>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <chrono>
#include <MatrixFunctionsThreads.h>
#include "ThreadPool.h"

class CholeskyThreads {
public:
    CholeskyThreads();

    struct CholeskyResultType {
        double time;
        double frobeniusNorm;
    };

    void calculateRowsLowerMatrix(double** source, double** lower, int size);

    void calculateColumnsLowerMatrix(double** array, double** lower, int size);

    /*
     * Run choleskyThreads algorithm (rows approach)
     * returns time consumed (double)
     */
    CholeskyResultType runCholeskyThreadsRowsAlgorithm(double** matrix, double* b, double* x, int size);

    /*
     * Run choleskyThreads algorithm (rows approach)
     * returns time consumed (double)
     */
    CholeskyResultType runCholeskyThreadsColumnsAlgorithm(double** matrix, double* b, double* x, int size);

private:
    ThreadPool threadPool_;

};