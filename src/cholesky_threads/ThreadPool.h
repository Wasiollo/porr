#pragma once

#include <vector>
#include <queue>
#include <memory>
#include <thread>
#include <mutex>
#include <condition_variable>
#include <future>
#include <functional>
#include <stdexcept>

class RunWhileStopped : public std::runtime_error {
	public:
	RunWhileStopped(const std::string& msg) : std::runtime_error(msg) {}
};

class ThreadPool {
public:
    ThreadPool(size_t);
    ~ThreadPool();

    template<class F, class... Args>
    std::future<typename std::result_of<F(Args...)>::type> run(F&& f, Args&&... args);
    size_t getThreadsNum(){
        return this->threadsNum;
    }

private:
    std::vector<std::thread> workers_;
    std::queue<std::function<void()>> tasksQueue_;
    std::mutex queueMutex_;
    std::condition_variable conditionVar_;
    bool stopFlag_;
    size_t threadsNum;
};
 
inline ThreadPool::ThreadPool(size_t threads)
    :   stopFlag_(false)
{
    this->threadsNum = threads;
    for(size_t i = 0; i < threads; ++i)
        workers_.emplace_back( [&] {
                while (true) {
                    std::function<void()> task;

                    {
                        std::unique_lock<std::mutex> lock(queueMutex_);
                        conditionVar_.wait(lock, [&] {
								return stopFlag_ || !tasksQueue_.empty();
							});
                        if (stopFlag_ && tasksQueue_.empty()) {
                            return;
						}
                        task = std::move(tasksQueue_.front());
                        tasksQueue_.pop();
                    }

                    task();
                }
            }
        );
}

inline ThreadPool::~ThreadPool()
{
    {
        std::unique_lock<std::mutex> lock(queueMutex_);
        stopFlag_ = true;
    }
    conditionVar_.notify_all();
    for(std::thread &worker : workers_)
        worker.join();
}

template<class F, class... Args>
std::future<typename std::result_of<F(Args...)>::type> ThreadPool::run(F&& f, Args&&... args) 
{
    using resultType = typename std::result_of<F(Args...)>::type;

    auto task = std::make_shared< std::packaged_task<resultType()> >(
            std::bind(std::forward<F>(f), std::forward<Args>(args)...)
        );
        
    std::future<resultType> res = task->get_future();
    {
        std::unique_lock<std::mutex> lock(queueMutex_);

        if(stopFlag_) {
            throw RunWhileStopped("Tried to run task while ThreadPool was stopped");
		}

        tasksQueue_.emplace([task](){ (*task)(); });
    }
    conditionVar_.notify_one();
    return res;
}


