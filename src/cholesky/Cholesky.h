#pragma once

#include <cstdio>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <chrono>
#include <MatrixFunctions.h>

class Cholesky {
public:
    Cholesky();

    struct CholeskyResultType {
        double time;
        double frobeniusNorm;
    };

    void calculateRowsLowerMatrix(double** source, double** lower, int size);

    void calculateColumnsLowerMatrix(double** array, double** lower, int size);

    /*
     * Run cholesky algorithm (rows approach)
     * returns time consumed (double)
     */
    CholeskyResultType runCholeskyRowsAlgorithm(double** matrix, double* b, double* x, int size);

    /*
     * Run cholesky algorithm (rows approach)
     * returns time consumed (double)
     */
    CholeskyResultType runCholeskyColumnsAlgorithm(double** matrix, double* b, double* x, int size);
};