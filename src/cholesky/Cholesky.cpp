#include "Cholesky.h"

Cholesky::Cholesky() = default;

void Cholesky::calculateColumnsLowerMatrix(double** array, double** lower, int size) {
    MatrixFunctions functions;
    for (int k = 0; k < size - 1; k++) {
        lower[k][k] = sqrt(std::abs(array[k][k]));

        for (int i = k + 1; i < size; i++) {
            lower[i][k] = array[i][k] / lower[k][k];
        }

        for (int j = k + 1; j < size; j++) {
            for (int i = j; i < size; i++) {
                array[i][j] -= lower[i][k] * lower[j][k];
            }
        }
    }
    lower[size - 1][size - 1] = sqrt(std::abs(array[size - 1][size - 1]));
}

Cholesky::CholeskyResultType Cholesky::runCholeskyColumnsAlgorithm(double **matrix, double *b, double *x, int size)  {
    MatrixFunctions matrixFunctions;

    // allocation of memory
    double** factorizedLowerMatrix = matrixFunctions.allocateMatrix(size);
    double** factorizedLowerMatrixTransposed = matrixFunctions.allocateMatrix(size);
    double* y = matrixFunctions.allocateArray(size);
    double ** matrixCopy = matrixFunctions.allocateMatrix(size);
    // allocation matrices for frobenius norm calculation
    auto errorMatrix = matrixFunctions.allocateMatrix(size);
    auto lltMatrix = matrixFunctions.allocateMatrix(size); // L*L^T matrix

    matrixFunctions.deepCopyMatrix(matrix, matrixCopy, size);

    /* START counting time for Cholesky columns method */
    auto choleskyColumnsStartTime = std::chrono::high_resolution_clock::now();

    /* CORE PART OF CALCULATION */
    this->calculateColumnsLowerMatrix(matrix, factorizedLowerMatrix, size);

    /* END counting time for Cholesky columns method */
    auto choleskyColumnsEndTime = std::chrono::high_resolution_clock::now();
    auto choleskyColumnsDuration = std::chrono::duration_cast<std::chrono::microseconds>(choleskyColumnsEndTime - choleskyColumnsStartTime);

    matrixFunctions.transposeMatrix(factorizedLowerMatrix, factorizedLowerMatrixTransposed, size);

    matrixFunctions.multiplyMatrix(factorizedLowerMatrix, factorizedLowerMatrixTransposed, lltMatrix, size);
    matrixFunctions.subtractMatrices(matrixCopy, lltMatrix, errorMatrix, size);

    auto frobeniusNorm = matrixFunctions.frobeniusNorm(errorMatrix, size);

    // deallocation of memory
    matrixFunctions.deallocateMatrix(lltMatrix, size);
    matrixFunctions.deallocateMatrix(errorMatrix, size);
    matrixFunctions.deallocateArray(y);
    matrixFunctions.deallocateMatrix(factorizedLowerMatrixTransposed, size);
    matrixFunctions.deallocateMatrix(factorizedLowerMatrix, size);

    auto time =  choleskyColumnsDuration.count();

    CholeskyResultType result;
    result.time = time;
    result.frobeniusNorm = frobeniusNorm;
    return result;
}

void Cholesky::calculateRowsLowerMatrix(double **source, double **lower, int size)  {
    for (int i = 0; i < size; i++) {
        for (int j = 0; j <= i; j++) {
            double sum = 0;

            if (j == i) {
                for (int k = 0; k < j; k++){
                    sum += pow(lower[j][k], 2);
                }
                lower[j][j] = sqrt(std::abs(source[j][j] - sum));
            } else {
                for (int k = 0; k < j; k++) {
                    sum += (lower[i][k] * lower[j][k]);
                }
                lower[i][j] = (source[i][j] - sum) / lower[j][j];
            }

        }
    }
}

Cholesky::CholeskyResultType Cholesky::runCholeskyRowsAlgorithm(double **matrix, double *b, double *x, int size) {
    MatrixFunctions matrixFunctions;

    // allocation of memory
    double** factorizedLowerMatrix = matrixFunctions.allocateMatrix(size);
    double** factorizedLowerMatrixTransposed = matrixFunctions.allocateMatrix(size);
    double* y = matrixFunctions.allocateArray(size);
    // allocation matrices for frobenius norm calculation
    auto errorMatrix = matrixFunctions.allocateMatrix(size);
    auto lltMatrix = matrixFunctions.allocateMatrix(size); // L*L^T matrix

    /* START counting time for Cholesky rows method */
    auto choleskyRowsStartTime = std::chrono::high_resolution_clock::now();

    /* CORE PART OF CALCULATION */
    this->calculateRowsLowerMatrix(matrix, factorizedLowerMatrix, size);

    /* END counting time for Cholesky rows method */
    auto choleskyRowsEndTime = std::chrono::high_resolution_clock::now();
    auto choleskyRowsDuration = std::chrono::duration_cast<std::chrono::microseconds>(choleskyRowsEndTime - choleskyRowsStartTime);

    matrixFunctions.transposeMatrix(factorizedLowerMatrix, factorizedLowerMatrixTransposed, size);
    matrixFunctions.multiplyMatrix(factorizedLowerMatrix, factorizedLowerMatrixTransposed, lltMatrix, size);
    matrixFunctions.subtractMatrices(matrix, lltMatrix, errorMatrix, size);

    auto frobeniusNorm = matrixFunctions.frobeniusNorm(errorMatrix, size);


    // deallocation of memory
    matrixFunctions.deallocateMatrix(lltMatrix, size);
    matrixFunctions.deallocateMatrix(errorMatrix, size);
    matrixFunctions.deallocateArray(y);
    matrixFunctions.deallocateMatrix(factorizedLowerMatrixTransposed, size);
    matrixFunctions.deallocateMatrix(factorizedLowerMatrix, size);

    auto time =  choleskyRowsDuration.count();

    CholeskyResultType result;
    result.time = time;
    result.frobeniusNorm = frobeniusNorm;
    return result;
}
