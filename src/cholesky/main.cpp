#include <iostream>
#include <string>
#include <chrono>

#include <Cholesky.h>
#include <MatrixFunctions.h>

int main(int argc, const char *argv[]) {
    Cholesky cholesky;
    MatrixFunctions matrixFunctions;

    int matrixSize = 46;
    int numberOfMatrices = 1;
    bool csvOutputType = false;

    if (argc > 1) {
        matrixSize = atoi(argv[1]);
    }
    if (argc > 2) {
        numberOfMatrices = atoi(argv[2]);
    }
    if (argc > 3) {
        if (atoi(argv[3]) == 1) {
            csvOutputType = true;
        }
    }

    double rowsTimeConsumed = 0;
    double rowsFrobeniusNorm = 0;
    double columnsTimeConsumed = 0;
    double columnsFrobeniusNorm = 0;

    /* MEMORY ALLOCATION */
    double ***arrayOfSourceMatricesForColumnsMethod = matrixFunctions.allocateNMatrices(numberOfMatrices, matrixSize);
    double ***arrayOfSourceMatricesForRowsMethod = matrixFunctions.allocateNMatrices(numberOfMatrices, matrixSize);
    double **arrayOfBArrays = matrixFunctions.allocateNArrays(numberOfMatrices, matrixSize); // can be reused
    double **arrayOfResultArraysForColumnsMethod = matrixFunctions.allocateNArrays(numberOfMatrices, matrixSize);
    double **arrayOfResultArraysForRowsMethod = matrixFunctions.allocateNArrays(numberOfMatrices, matrixSize);

    for (int i = 0; i < numberOfMatrices; ++i) {
        /* GENERATE MATRIX */
        matrixFunctions.loadMatrixFromFile(arrayOfSourceMatricesForColumnsMethod[i], matrixSize);
        /* DEEP COPY */
        matrixFunctions.deepCopyMatrix(arrayOfSourceMatricesForColumnsMethod[i], arrayOfSourceMatricesForRowsMethod[i], matrixSize);
        /* GENERATE B ARRAY */
        matrixFunctions.generateBArray(arrayOfBArrays[i], matrixSize);
    }

    /* ROWS METHOD EXECUTION */
    for (int i = 0; i < numberOfMatrices; ++i) {
        auto result = cholesky.runCholeskyRowsAlgorithm(arrayOfSourceMatricesForRowsMethod[i],
                                                              arrayOfBArrays[i],
                                                              arrayOfResultArraysForRowsMethod[i],
                                                              matrixSize);
        rowsTimeConsumed += result.time;
        rowsFrobeniusNorm += result.frobeniusNorm;
    }

    /* COLUMNS METHOD EXECUTION */
    for (int i = 0; i < numberOfMatrices; ++i) {
        auto result = cholesky.runCholeskyColumnsAlgorithm(arrayOfSourceMatricesForColumnsMethod[i],
                                                             arrayOfBArrays[i],
                                                             arrayOfResultArraysForColumnsMethod[i],
                                                             matrixSize);
        columnsTimeConsumed += result.time;
        columnsFrobeniusNorm += result.frobeniusNorm;
    }

    /* MEMORY DEALLOCATION */
    matrixFunctions.deallocateNArrays(arrayOfResultArraysForRowsMethod, numberOfMatrices);
    matrixFunctions.deallocateNArrays(arrayOfResultArraysForColumnsMethod, numberOfMatrices);
    matrixFunctions.deallocateNArrays(arrayOfBArrays, numberOfMatrices);
    matrixFunctions.deallocateNMatrices(arrayOfSourceMatricesForRowsMethod, numberOfMatrices, matrixSize);
    matrixFunctions.deallocateNMatrices(arrayOfSourceMatricesForColumnsMethod, numberOfMatrices, matrixSize);

    /* RESULT PRINTING */
    if(!csvOutputType){
        std::cout << "Cholesky factorization (columns method) take " << columnsTimeConsumed << " microseconds for "
                  << numberOfMatrices << " matrices with size " << matrixSize << "x" << matrixSize
                  << ". Per matrix: " << columnsTimeConsumed / numberOfMatrices << " microseconds."
                  << " Average Frobenius Norm: " << columnsFrobeniusNorm / numberOfMatrices << std::endl;

        std::cout << "Cholesky factorization (rows method) take " << rowsTimeConsumed << " microseconds for "
                  << numberOfMatrices << " matrices with size " << matrixSize << "x" << matrixSize
                  << ". Per matrix: " << rowsTimeConsumed / numberOfMatrices << " microseconds."
                  << " Average Frobenius Norm: " << rowsFrobeniusNorm / numberOfMatrices << std::endl;

    } else {
        std::cout << "Cholesky factorization (columns method)," << columnsTimeConsumed << ","
                  << numberOfMatrices << "," << matrixSize << "x" << matrixSize
                  << "," << columnsTimeConsumed / numberOfMatrices << "," << columnsFrobeniusNorm / numberOfMatrices
                  << std::endl;

        std::cout << "Cholesky factorization (rows method)," << rowsTimeConsumed << ","
                  << numberOfMatrices << "," << matrixSize << "x" << matrixSize
                  << "," << rowsTimeConsumed / numberOfMatrices << "," << rowsFrobeniusNorm / numberOfMatrices
                  << std::endl;
    }
    return 0;
}
