#include <iostream>
#include <string>
#include <chrono>
#include <CholeskyVectorized.h>

#include <MatrixFunctionsVectorized.h>

int main(int argc, const char *argv[]) {
    CholeskyVectorized choleskyVectorized;
    MatrixFunctionsVectorized functions;

    int matrixSize = 500;
    int numberOfMatrices = 1;
    bool csvOutputType = false;

    if (argc > 1) {
        matrixSize = atoi(argv[1]);
    }
    if (argc > 2) {
        numberOfMatrices = atoi(argv[2]);
    }
    if (argc > 3) {
        if (atoi(argv[3]) == 1) {
            csvOutputType = true;
        }
    }

    double rowsTimeConsumed = 0;
    double rowsFrobeniusNorm = 0;
    double columnsTimeConsumed = 0;
    double columnsFrobeniusNorm = 0;

    /* MEMORY ALLOCATION */
    double ***arrayOfSourceMatricesForColumnsMethod = functions.allocateNMatrices(numberOfMatrices, matrixSize);
    double ***arrayOfSourceMatricesForRowsMethod = functions.allocateNMatrices(numberOfMatrices, matrixSize);
    double **arrayOfBArrays = functions.allocateNArrays(numberOfMatrices, matrixSize); // can be reused
    double **arrayOfResultArraysForColumnsMethod = functions.allocateNArrays(numberOfMatrices, matrixSize);
    double **arrayOfResultArraysForRowsMethod = functions.allocateNArrays(numberOfMatrices, matrixSize);

    for (int i = 0; i < numberOfMatrices; ++i) {
        /* GENERATE MATRIX */
        functions.loadMatrixFromFile(arrayOfSourceMatricesForColumnsMethod[i], matrixSize);
        /* DEEP COPY */
        functions.deepCopyMatrix(arrayOfSourceMatricesForColumnsMethod[i], arrayOfSourceMatricesForRowsMethod[i], matrixSize);
        /* GENERATE B ARRAY */
        functions.generateBArray(arrayOfBArrays[i], matrixSize);
    }

    /* ROWS METHOD EXECUTION */
    for (int i = 0; i < numberOfMatrices; ++i) {
        auto result = choleskyVectorized.runCholeskyVectorizedRowsAlgorithm(arrayOfSourceMatricesForRowsMethod[i],
                                                              arrayOfBArrays[i],
                                                              arrayOfResultArraysForRowsMethod[i],
                                                              matrixSize);
        rowsTimeConsumed += result.time;
        rowsFrobeniusNorm += result.frobeniusNorm;
    }

    /* COLUMNS METHOD EXECUTION */
    for (int i = 0; i < numberOfMatrices; ++i) {
        auto result = choleskyVectorized.runCholeskyVectorizedColumnsAlgorithm(arrayOfSourceMatricesForColumnsMethod[i],
                                                             arrayOfBArrays[i],
                                                             arrayOfResultArraysForColumnsMethod[i],
                                                             matrixSize);
        columnsTimeConsumed += result.time;
        columnsFrobeniusNorm += result.frobeniusNorm;
    }

    /* MEMORY DEALLOCATION */
    functions.deallocateNArrays(arrayOfResultArraysForRowsMethod, numberOfMatrices);
    functions.deallocateNArrays(arrayOfResultArraysForColumnsMethod, numberOfMatrices);
    functions.deallocateNArrays(arrayOfBArrays, numberOfMatrices);
    functions.deallocateNMatrices(arrayOfSourceMatricesForRowsMethod, numberOfMatrices, matrixSize);
    functions.deallocateNMatrices(arrayOfSourceMatricesForColumnsMethod, numberOfMatrices, matrixSize);

    /* RESULT PRINTING */
    if(!csvOutputType){
        std::cout << "CholeskyVectorized factorization (columns method) take " << columnsTimeConsumed << " microseconds for "
                  << numberOfMatrices << " matrices with size " << matrixSize << "x" << matrixSize
                  << ". Per matrix: " << columnsTimeConsumed / numberOfMatrices << " microseconds."
                 << " Average Frobenius Norm: " << columnsFrobeniusNorm / numberOfMatrices << std::endl;

        std::cout << "CholeskyVectorized factorization (rows method) take " << rowsTimeConsumed << " microseconds for "
                  << numberOfMatrices << " matrices with size " << matrixSize << "x" << matrixSize
                  << ". Per matrix: " << rowsTimeConsumed / numberOfMatrices << " microseconds."
                  << " Average Frobenius Norm: " << rowsFrobeniusNorm / numberOfMatrices << std::endl;

    } else {
        std::cout << "CholeskyVectorized factorization (columns method)," << columnsTimeConsumed << ","
                  << numberOfMatrices << "," << matrixSize << "x" << matrixSize
                  << "," << columnsTimeConsumed / numberOfMatrices << columnsFrobeniusNorm / numberOfMatrices
                  << std::endl;

        std::cout << "CholeskyVectorized factorization (rows method)," << rowsTimeConsumed << ","
                  << numberOfMatrices << "," << matrixSize << "x" << matrixSize
                  << "," << rowsTimeConsumed / numberOfMatrices << rowsFrobeniusNorm / numberOfMatrices
                  << std::endl;
    }
    return 0;
}
