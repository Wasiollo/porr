#include "CholeskyVectorized.h"

CholeskyVectorized::CholeskyVectorized() = default;

void CholeskyVectorized::calculateColumnsLowerMatrix(double** array, double** lower, int size) {
    for (int k = 0; k < size - 1; k++) {
        lower[k][k] = sqrt(array[k][k]);

        for (int i = k + 1; i < size; i++) {
            lower[i][k] = array[i][k] / lower[k][k];
        }

        for (int j = k + 1; j < size; j++) {
            for (int i = j; i < size; i++) {
                array[i][j] -= lower[i][k] * lower[j][k];
            }
        }
    }
    lower[size - 1][size - 1] = sqrt(array[size - 1][size - 1]);
}

CholeskyVectorized::CholeskyResultType CholeskyVectorized::runCholeskyVectorizedColumnsAlgorithm(double **matrix, double *b, double *x, int size)  {
    MatrixFunctionsVectorized matrixFunctionsVectorized;

    // allocation of memory
    double** factorizedLowerMatrix = matrixFunctionsVectorized.allocateMatrix(size);
    double** factorizedLowerMatrixTransposed = matrixFunctionsVectorized.allocateMatrix(size);
    double* y = matrixFunctionsVectorized.allocateArray(size);
    double ** matrixCopy = matrixFunctionsVectorized.allocateMatrix(size);
    // allocation matrices for frobenius norm calculation
    auto errorMatrix = matrixFunctionsVectorized.allocateMatrix(size);
    auto lltMatrix = matrixFunctionsVectorized.allocateMatrix(size); // L*L^T matrix

    matrixFunctionsVectorized.deepCopyMatrix(matrix, matrixCopy, size);

    /* START counting time for CholeskyVectorized columns method */
    auto choleskyVectorizedColumnsStartTime = std::chrono::high_resolution_clock::now();

    /* CORE PART OF CALCULATION */
    this->calculateColumnsLowerMatrix(matrix, factorizedLowerMatrix, size);

    /* END counting time for CholeskyVectorized columns method */
    auto choleskyVectorizedColumnsEndTime = std::chrono::high_resolution_clock::now();
    auto choleskyVectorizedColumnsDuration = std::chrono::duration_cast<std::chrono::microseconds>(choleskyVectorizedColumnsEndTime - choleskyVectorizedColumnsStartTime);

    matrixFunctionsVectorized.transposeMatrix(factorizedLowerMatrix, factorizedLowerMatrixTransposed, size);
    matrixFunctionsVectorized.multiplyMatrix(factorizedLowerMatrix, factorizedLowerMatrixTransposed, lltMatrix, size);
    matrixFunctionsVectorized.subtractMatrices(matrixCopy, lltMatrix, errorMatrix, size);

    auto frobeniusNorm = matrixFunctionsVectorized.frobeniusNorm(errorMatrix, size);

    // deallocation of memory
    matrixFunctionsVectorized.deallocateMatrix(lltMatrix, size);
    matrixFunctionsVectorized.deallocateMatrix(errorMatrix, size);
    matrixFunctionsVectorized.deallocateArray(y);
    matrixFunctionsVectorized.deallocateMatrix(factorizedLowerMatrixTransposed, size);
    matrixFunctionsVectorized.deallocateMatrix(factorizedLowerMatrix, size);

    auto time =  choleskyVectorizedColumnsDuration.count();

    CholeskyResultType result;
    result.time = time;
    result.frobeniusNorm = frobeniusNorm;
    return result;
}

void CholeskyVectorized::calculateRowsLowerMatrix(double **source, double **lower, int size)  {
    for (int i = 0; i < size; i++) {
        for (int j = 0; j <= i; j++) {
            double sum = 0;

            if (j == i) {
                for (int k = 0; k < j; k++){
                    sum += pow(lower[j][k], 2);
                }
                lower[j][j] = sqrt(source[j][j] - sum);
            } else {
                for (int k = 0; k < j; k++) {
                    sum += (lower[i][k] * lower[j][k]);
                }
                lower[i][j] = (source[i][j] - sum) / lower[j][j];
            }

        }
    }
}

CholeskyVectorized::CholeskyResultType CholeskyVectorized::runCholeskyVectorizedRowsAlgorithm(double **matrix, double *b, double *x, int size) {
    MatrixFunctionsVectorized matrixFunctionsVectorized;

    // allocation of memory
    double** factorizedLowerMatrix = matrixFunctionsVectorized.allocateMatrix(size);
    double** factorizedLowerMatrixTransposed = matrixFunctionsVectorized.allocateMatrix(size);
    double* y = matrixFunctionsVectorized.allocateArray(size);
    // allocation matrices for frobenius norm calculation
    auto errorMatrix = matrixFunctionsVectorized.allocateMatrix(size);
    auto lltMatrix = matrixFunctionsVectorized.allocateMatrix(size); // L*L^T matrix

    /* START counting time for CholeskyVectorized rows method */
    auto choleskyVectorizedRowsStartTime = std::chrono::high_resolution_clock::now();

    /* CORE PART OF CALCULATION */
    this->calculateRowsLowerMatrix(matrix, factorizedLowerMatrix, size);


    /* END counting time for CholeskyVectorized rows method */
    auto choleskyVectorizedRowsEndTime = std::chrono::high_resolution_clock::now();
    auto choleskyVectorizedRowsDuration = std::chrono::duration_cast<std::chrono::microseconds>(choleskyVectorizedRowsEndTime - choleskyVectorizedRowsStartTime);

    matrixFunctionsVectorized.transposeMatrix(factorizedLowerMatrix, factorizedLowerMatrixTransposed, size);
    matrixFunctionsVectorized.multiplyMatrix(factorizedLowerMatrix, factorizedLowerMatrixTransposed, lltMatrix, size);
    matrixFunctionsVectorized.subtractMatrices(matrix, lltMatrix, errorMatrix, size);

    auto frobeniusNorm = matrixFunctionsVectorized.frobeniusNorm(errorMatrix, size);

    // deallocation of memory
    matrixFunctionsVectorized.deallocateMatrix(lltMatrix, size);
    matrixFunctionsVectorized.deallocateMatrix(errorMatrix, size);
    matrixFunctionsVectorized.deallocateArray(y);
    matrixFunctionsVectorized.deallocateMatrix(factorizedLowerMatrixTransposed, size);
    matrixFunctionsVectorized.deallocateMatrix(factorizedLowerMatrix, size);

    auto time =  choleskyVectorizedRowsDuration.count();

    CholeskyResultType result;
    result.time = time;
    result.frobeniusNorm = frobeniusNorm;
    return result;
}
