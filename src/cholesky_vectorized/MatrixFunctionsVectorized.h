#pragma once


#include <cstdio>
#include <cstdlib>
#include <cmath>

class MatrixFunctionsVectorized {
public:
    MatrixFunctionsVectorized();

    double *allocateArray(int size);

    double **allocateMatrix(int size);

    double ***allocateNMatrices(int n, int size);

    double **allocateNArrays(int n, int size);

    void deallocateArray(const double *array);

    void deallocateMatrix(double **matrix, int size);

    void deallocateNMatrices(double ***nMatrices, int n, int size);

    void deallocateNArrays(double **nArrays, int n);

    void multiplyMatrix(double **a, double **b, double **result, int size);

    void transposeMatrix(double **source, double **result, int size);

    void loadMatrixFromFile(double **result, int size);

    void generateBArray(double *result, int size);

    void printArray(double *array, int size);

    void printMatrix(double **matrix, int size);

    double randValue();

    void deepCopyArray(double *source, double *target, int size);

    void deepCopyMatrix(double **source, double **target, int size);

    void subtractMatrices(double **a, double **b, double **result, int size);

    double frobeniusNorm(double **matrix, int size);
};
