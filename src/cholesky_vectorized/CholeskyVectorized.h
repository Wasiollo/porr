#pragma once

#include <cstdio>
#include <cmath>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <chrono>
#include <MatrixFunctionsVectorized.h>

class CholeskyVectorized {
public:
    CholeskyVectorized();

    struct CholeskyResultType {
        double time;
        double frobeniusNorm;
    };

    void calculateRowsLowerMatrix(double** source, double** lower, int size);

    void calculateColumnsLowerMatrix(double** array, double** lower, int size);

    /*
     * Run choleskyVectorized algorithm (rows approach)
     * returns time consumed (double)
     */
    CholeskyResultType runCholeskyVectorizedRowsAlgorithm(double** matrix, double* b, double* x, int size);

    /*
     * Run choleskyVectorized algorithm (rows approach)
     * returns time consumed (double)
     */
    CholeskyResultType runCholeskyVectorizedColumnsAlgorithm(double** matrix, double* b, double* x, int size);
};