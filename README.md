# Cholesky factorization - comparison of the effectiveness of the row and column versions

[![pipeline status](https://gitlab.com/Wasiollo/porr/badges/master/pipeline.svg)](https://gitlab.com/Wasiollo/porr/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://gitlab.com/Wasiollo/porr/-/raw/master/LICENSE)

Project for PORR lessons on WUT, which shows Cholesky factorization - comparison of the effectiveness of the row and column versions
 
 ## Getting Started
 
 These instructions will get you a copy of the project up and running on your local machine for development purposes.
 
 ### Prerequisites
 
 What things you need to install the software and how to install them
 ```
 - cmake 3.16.3
 - g++  9.3.0
 ```
 
 ### Installing
 
 A step by step series of examples that tell you how to get a development env running
 
 Installing dependencies (root directory of project)
 
 ```
mkdir build
cd build
cmake ..
make
 ```
 

 ## Built With
 * [CMake](https://cmake.org/) - Build system
 * [gtest](https://github.com/google/googletest) - Test framework
 
 ## Authors
 
 * **Mateusz Wasiak**
 * **Mateusz Plesiński**
 
 See the list of [contributors](https://gitlab.com/Wasiollo/porr/-/graphs/master) who participated in this project.
 
 ## License
 
 This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

 
